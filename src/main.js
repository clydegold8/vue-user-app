import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App.vue'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueRouter)
Vue.use(VueResource);

const ListUsers = require('./js/list-users.vue');
const EditUser = require('./js/edit-user.vue');
const AddUser = require('./js/add-users.vue');

const routes = [
    {
        name: 'list-users',
        path: '/',
        component: ListUsers
    },
    {
        name: 'edit-user',
        path: '/user/edit/:id',
        component: EditUser
    },
    {
        name: 'add-user',
        path: '/user/add/',
        component: AddUser
    }
];

var router = new VueRouter({ routes: routes, mode: 'history' });
new Vue(Vue.util.extend({ router }, App)).$mount('#app');